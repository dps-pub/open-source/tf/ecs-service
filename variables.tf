variable "environment" {
  description = "Name of the service"
  default     = "develop"
}

variable "service" {
  description = "Name of the service"
  default     = "agedcareguide.com.au"
}

variable "alias" {
  description = "Alias of the service"
  default     = "acg"
}

variable "log_retention" {
  default = "30"
}

variable "lb_security_groups" {
  description = "list of security groups to add"
  default     = []
}

variable "cluster" {}
variable "role" {}

variable "task" {
  default = "1"
}

variable "vpc_id" {
  description = "VPC Id"
  default     = "vpc-4f5bb12b"
}

variable "certificate_arn" {
  description = "certificate_arn"
  default     = ""
}
