resource "aws_lb" "this" {
  name               = "${var.alias}-${var.environment}-http"
  internal           = false
  load_balancer_type = "application"

  security_groups = ["${var.lb_security_groups}"]

  subnets                    = ["subnet-31b56c47", "subnet-a7ee24fe"]
  enable_deletion_protection = true

  tags {
    Service     = "${var.service}"
    Environment = "${var.environment}"
  }
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = "${aws_lb.this.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${var.certificate_arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.this.arn}"
    type             = "forward"
  }
}

resource "aws_cloudwatch_log_group" "this" {
  name              = "${var.alias}-${var.environment}"
  retention_in_days = "${var.log_retention}"

  tags {
    Service     = "${var.service}"
    Environment = "${var.environment}"
  }
}

resource "aws_lb_target_group" "this" {
  name     = "${var.alias}-${var.environment}-http"
  port     = 80
  protocol = "HTTPS"
  vpc_id   = "${var.vpc_id}"

  health_check {
    path                = "/status"
    protocol            = "HTTPS"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 25
    interval            = 30
  }
}

resource "aws_ecs_service" "this" {
  name            = "${var.alias}-${var.environment}-http"
  cluster         = "${var.cluster}"
  desired_count   = 1
  task_definition = "${var.alias}-${var.environment}-http:${var.task}"

  iam_role   = "${var.role}"
  depends_on = ["aws_lb.this"]

  load_balancer {
    target_group_arn = "${aws_lb_target_group.this.arn}"
    container_name   = "php"
    container_port   = 443
  }

  lifecycle {
    ignore_changes = ["task_definition", "desired_count"]
  }
}

# resource "aws_iam_role" "this" {
#   name = "${var.alias}-${var.environment}-http"


#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#       {
#           "Effect": "Allow",
#           "Action": [
#               "s3:Get*",
#               "s3:List*"
#           ],
#           "Resource": "*"
#       }
#   ]
# }
# EOF
# }

